var Metalsmith        = require('metalsmith');
var layouts           = require('metalsmith-layouts');
var inPlace           = require('metalsmith-in-place');
var browserSync       = require('metalsmith-browser-sync');
var filenames         = require('metalsmith-filenames');
var beautify          = require('metalsmith-beautify');
var ignore            = require('metalsmith-ignore');
var metadata          = require('metalsmith-metadata');

var sass              = require('metalsmith-sass');
var sassSourceAuto    = require('./plugins/metalsmith-sass-sourcemaps-autoprefixer');

Metalsmith(__dirname)
  .metadata({
    title: "MetalsmithStarterKit",
    description: "",
    generator: "Metalsmith",
    url: "http://www.metalsmith.io/"
  })

  .use(filenames())

  .use(metadata({
    items: 'data/items.yml'
  }))

  .source('src')
  .destination('build')
  .clean(true)

  .use(sassSourceAuto({
    browsers: ['last 2 versions', '> 1%'],
  }))

  .use(sass({
    sourceMap: true,
    sourceMapContents: true,
    outputDir: 'assets/css/',
    precision: 7,
    // onError: browserSync.notify,
    outputStyle: 'expanded'
  }))

  .use(layouts({
    engine: 'pug',
    directory: './src/layouts',
    // rename: true,
    // pretty: true,
  }))

  .use(inPlace({
    engine: 'pug',
    rename: true,
    pattern: '*.pug',
    // pretty: true,
  }))

  .use(beautify({
    "css": false,
    "js": false,
    "html": {
      "wrap_line_length": 0,
      "indent_size": 2,
      "indent_char": " ",
      // "unformatted": ["a", "abbr", "area", "audio", "b", "bdi", "bdo", "br", "button", "canvas", "cite", "code", "data", "datalist", "del", "dfn", "em", "embed", "i", "iframe", "img", "input", "ins", "kbd", "keygen", "label", "map", "mark", "math", "meter", "noscript", "object", "output", "progress", "q", "ruby", "s", "samp", "select", "small", "span", "strong", "sub", "sup", "svg", "template", "textarea", "time", "u", "var", "video", "wbr", "text"],
      "unformatted": [],
      // "extra_liners": ["head", "body", "/html", "section", "header", "main", "footer"],
      "extra_liners": [],
    }
  }))

  .use(browserSync({
    server: "build",
    open: false,
    files: [
      "src/**/*.*",
    ],
  }))

  .use(ignore([
    'layouts/*',
    'partials/*'
  ]))

  .build(function(err, files) {
    if (err) { throw err; }
  });
